(function() {
  'use strict';

  angular
    .module('crashio', ['ngResource', 'ui.router', 'ngMaterial', 'toastr', 'nvd3', 'md.data.table']);

})();
