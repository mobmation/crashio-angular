(function() {
  'use strict';

  angular
    .module('crashio')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('profiles', {
        url: '/profiles',
        views: {
          mainModule: {
            controller: 'MainController',
            controllerAs: 'main',
            templateUrl: 'app/main/main.html'
          },
          'leftSidePaneModule@profiles': {
            template: '<div>' +
            '  <div ui-view="leftWidgetOne"></div>' +
            '  <div ui-view="leftWidgetTwo"></div>' +
            '</div>'
          },
          'leftWidgetOne@profiles': {
            template: '<p></p>'
          },
          'chart@profiles': {
            templateUrl: 'app/main/chart/chart.html',
            controller: 'ChartController',
            controllerAs: 'chart'
          },
          'trade@profiles': {
            templateUrl: 'app/main/trade/trade.html',
            controller: 'TradeController',
            controllerAs: 'trade'
          },
          'portfolio@profiles': {
            templateUrl: 'app/main/portfolio/portfolio.html',
            controller: 'PortfolioController',
            controllerAs: 'portfolio'
          }
        }
      });

    $urlRouterProvider.otherwise('/');
  }

})();
