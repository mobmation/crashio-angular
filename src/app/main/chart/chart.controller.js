(function() {
  'use strict';

  angular
    .module('crashio')
    .controller('ChartController', ChartController);

  /** @ngInject */
  function ChartController($timeout, toastr, $filter, $interval, $q, $http, $log) {

    var vm = this;

    vm.test = 100;



    function csvParser( strData, strDelimiter ){
      // Check to see if the delimiter is defined. If not,
      // then default to comma.
      strDelimiter = (strDelimiter || ",");

      // Create a regular expression to parse the CSV values.
      var objPattern = new RegExp(
        (
          // Delimiters.
          "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
          "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
          "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
      );


      // Create an array to hold our data. Give the array
      // a default empty first row.
      var arrData = [[]];

      // Create an array to hold our individual pattern
      // matching groups.
      var arrMatches = null;


      // Keep looping over the regular expression matches
      // until we can no longer find a match.
      while (arrMatches = objPattern.exec( strData )){

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
          strMatchedDelimiter.length &&
          strMatchedDelimiter !== strDelimiter
        ){

          // Since we have reached a new row of data,
          // add an empty row to our data array.
          arrData.push( [] );

        }

        var strMatchedValue;

        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[ 2 ]){

          // We found a quoted value. When we capture
          // this value, unescape any double quotes.
          strMatchedValue = arrMatches[ 2 ].replace(
            new RegExp( "\"\"", "g" ),
            "\""
          );

        } else {

          // We found a non-quoted value.
          strMatchedValue = arrMatches[ 3 ];

        }


        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push( strMatchedValue );
      }

      // Return the parsed data.
      return( arrData );
    }

    vm.source = "/assets/charts/c1.csv";
    vm.items = $http.get(vm.source).then(function(response){
      $log.info(csvParser(response.data));
      var temp = csvParser(response.data);
      marketChart(temp,500);
      return csvParser(response.data);
    });





    function marketChart(csv, refresh_rate) {
      vm.options = {
        chart: {
          type: 'lineChart',
          height: 200,
          margin: {
            top: 20,
            right: 10,
            bottom: 40,
            left: 45
          },
          x: function (d) {
            return d.x;
          },
          y: function (d) {
            return d.y;
          },
          useInteractiveGuideline: true,
          duration: 500,
          xAxis: {
            tickFormat: function (d) {
              return d3.time.format.utc(timeFormat)(new Date(d)); // tickMultiFormat(new Date(d));
            },
            axisLabel: ''
          },
          showXAxis: true,
          yAxis: {
            tickFormat: function (d) {
              return d3.format('.01f')(d);
            },
            axisLabel: '',
            axisLabelDistance: -15
          }
        }
      };

      var timeFormat = '%b %e ';  // '%b %e %H:%M:%S'
      var tickMultiFormat = d3.time.format.multi([
        ["%-I:%M%p", function(d) { return d.getMinutes(); }], // not the beginning of the hour
        ["%-I%p", function(d) { return d.getHours(); }], // not midnight
        ["%b %-d", function(d) { return d.getDate() != 1; }], // not the first of the month
        ["%b %-d", function(d) { return d.getMonth(); }], // not Jan 1st
        ["%Y", function() { return true; }]
      ]);

      vm.data = [{values: [], key: 'Price', color: "0FB825"}];

      vm.interval = $interval (function (){},5500);

      // to remove the first & last elements
      csv.shift();
      csv.pop();

/*      angular.forEach(csv, function(item){
        var refresh = (function(){
          var timestamp = new Date(item[0]).getTime();
          //var timestamp = $filter('date')(item.timestamp,'YYYY-MM-dd');
          $log.info(item[0]);
          vm.data[0].values.push({x: timestamp, y: item[1]});
          $log.info('Wait');
          vm.test += 100;
        });

        $timeout(refresh, 1000);
        $log.info('Proceed');
      });*/

      var update = $interval(function () {
        if(csv.length != 0){
          var timestamp = new Date(csv[0][0]).getTime();
          vm.data[0].values.push({x: timestamp, y: csv[0][1]});
          $log.info();
          csv.shift();
        }
      }, refresh_rate);



    }

  }
})();
