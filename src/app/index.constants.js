/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('crashio')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
