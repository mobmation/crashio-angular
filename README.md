# README #

This project uses angularJs with angular-material as a base for the visual elements.

### Setup Instructions ###

* `npm install`
* `bower install`

### To run ###

* `gulp build`
* `gulp serve`